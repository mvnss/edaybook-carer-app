﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using EdaybookService.DataObjects;
using EdaybookService.Models;


namespace EdaybookService
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            // Use this class to set configuration options for your mobile service
            ConfigOptions options = new ConfigOptions();

            // Use this class to set WebAPI configuration options
            HttpConfiguration config = ServiceConfig.Initialize(new ConfigBuilder(options));

            // To display errors in the browser during development, uncomment the following
            // line. Comment it out again when you deploy your service for production use.
            // config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            
            Database.SetInitializer(new EdaybookInitializer());
        }
    }

    public class EdaybookInitializer : ClearDatabaseSchemaIfModelChanges<EdaybookContext>
    {
        protected override void Seed(EdaybookContext context)
        {
            //List<TodoItem> todoItems = new List<TodoItem>
            //{
            //    new TodoItem { Id = Guid.NewGuid().ToString(), Text = "First item", Complete = false },
            //    new TodoItem { Id = Guid.NewGuid().ToString(), Text = "Second item", Complete = false },
            //};

            //foreach (TodoItem todoItem in todoItems)
            //{
            //    context.Set<TodoItem>().Add(todoItem);
            //}

            List<Client> clients = new List<Client>
            {
                new Client { Id = Guid.NewGuid().ToString(), Title = "Mr", Firstname = "Paul", Lastname = "Adams", Deleted = false},
                new Client { Id = Guid.NewGuid().ToString(), Title = "Mr", Firstname = "John", Lastname = "Smith", Deleted = false},
            };

            foreach (Client client in clients)
            {
                context.Set<Client>().Add(client);
            }

            base.Seed(context);
        }
    }
}

