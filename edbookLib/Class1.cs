﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Windows.Storage;
using System.Threading.Tasks;
namespace edbookLib
{
    public class ImageConverter
    {
        public static async Task<byte[]> imageToByteArray(Uri sourceUri)
        {
            var file = await StorageFile.GetFileFromApplicationUriAsync(sourceUri);
            using (var inputStream = await file.OpenSequentialReadAsync())
            {
                var readStream = inputStream.AsStreamForRead();
                var buffer = new byte[readStream.Length];
                await readStream.ReadAsync(buffer, 0, buffer.Length);
                return buffer;
            }
        }

    }
}
