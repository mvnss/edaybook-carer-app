﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.Pickers;
using System.IO;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;


namespace Edaybook
{
    sealed partial class ClientGui: Templates.NavigationPage
    {
        private MobileServiceCollection<Client, Client> items;
        private IMobileServiceTable<Client> clientTable = App.MobileService.GetTable<Client>();

        Stream imageStream = null;
        
        public ClientGui()
        {
            this.InitializeComponent();
            DataContext = new MainViewmodel();
            
        }

        private async void ButtonCaptureImage_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".cmp");
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".tif");

            StorageFile file = await openPicker.PickSingleFileAsync();
            if (null != file)
            {
                try
                {

                    //var stream = await file.OpenStreamForReadAsync();

                    imageStream = await file.OpenStreamForReadAsync(); 
                }
                catch (Exception exception)
                {
                }
            }

        }

        private async Task InsertTodoItem(Client client)
        {
            // This code inserts a new TodoItem into the database. When the operation completes
            // and Mobile Services has assigned an Id, the item is added to the CollectionView

            string errorString = string.Empty;

            if (imageStream != null)
            {
                // Set blob properties of TodoItem.
                client.ContainerName = "todoitemimages";
                client.ResourceName = Guid.NewGuid().ToString() + ".jpg";
            }

            // Send the item to be inserted. When blob properties are set this
            // generates an SAS in the response.
            await clientTable.InsertAsync(client);

            
            // If we have a returned SAS, then upload the blob.
            if (!string.IsNullOrEmpty(client.SasQueryString))
            {
                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                StorageCredentials cred = new StorageCredentials(client.SasQueryString);
                var imageUri = new Uri(client.ImageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                CloudBlobContainer container = new CloudBlobContainer(
                    new Uri(string.Format("https://{0}/{1}",
                        imageUri.Host, client.ContainerName)), cred);

                // Upload the new image as a BLOB from the stream.
                CloudBlockBlob blobFromSASCredential =
                    container.GetBlockBlobReference(client.ResourceName);
                await blobFromSASCredential.UploadFromStreamAsync(imageStream.AsInputStream());

                // When you request an SAS at the container-level instead of the blob-level,
                // you are able to upload multiple streams using the same container credentials.

                imageStream = null;
            }



            items.Add(client);
        }

        private async Task RefreshTodoItems()
        {
            MobileServiceInvalidOperationException exception = null;
            try
            {

                // Handle the flipview here.
                Navflipview = (FlipView)this.FindName("flip");
                var Test = Navflipview.Items;

                // This code refreshes the entries in the list view by querying the TodoItems table.
                // The query excludes completed TodoItems
                items = await clientTable
                    .ToCollectionAsync();


            }
            catch (MobileServiceInvalidOperationException e)
            {
                exception = e;
            }

            if (exception != null)
            {
                await new MessageDialog(exception.Message, "Error loading items").ShowAsync();
            }
            else
            {
                //ListItems.ItemsSource = items;
                //this.ButtonSave.IsEnabled = true;
            }
        }

        private async Task UpdateCheckedTodoItem(Client item)
        {
            // This code takes a freshly completed TodoItem and updates the database. When the MobileService 
            // responds, the item is removed from the list 
            await clientTable.UpdateAsync(item);
            items.Remove(item);
            //ListItems.Focus(Windows.UI.Xaml.FocusState.Unfocused);
        }

        private async void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            await RefreshTodoItems();
        }

        private async void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            var todoItem = new Client {  Firstname = ""/*TextInput.Text */};
            await InsertTodoItem(todoItem);
        }

        private async void CheckBoxComplete_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            Client item = cb.DataContext as Client;
            await UpdateCheckedTodoItem(item);
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            //await RefreshTodoItems();
        }
    }
}
