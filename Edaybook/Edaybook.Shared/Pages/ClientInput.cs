﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.WindowsAzure.MobileServices;
using Windows.Storage;
using Windows.Storage.Pickers;
using System.IO;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Edaybook.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ClientInput : Templates.NavigationPage
    {
        public ClientInput()
        {
            this.InitializeComponent();
            DataContext = new MainViewmodel();
        }


        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.HomeGroup;
            openPicker.FileTypeFilter.Add(".png");
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".tif");
            
            StorageFile file = await openPicker.PickSingleFileAsync();
            if (null != file)
            {
                try
                {
                    ((MainViewmodel)DataContext).ImageStream = await file.OpenStreamForReadAsync();
                }
                catch (Exception exception)
                {
                    // to do
                }
            }
        }

        private async void Button_Save(object sender, RoutedEventArgs e)
        {
            await ((MainViewmodel)DataContext).Insert();
        }
    }
}
