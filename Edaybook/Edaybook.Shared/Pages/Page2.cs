﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Edaybook.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    sealed partial class Page2 : Edaybook.Templates.NavigationPage
    {
        public Page2()
        {
            this.InitializeComponent();
        }
    }
}
