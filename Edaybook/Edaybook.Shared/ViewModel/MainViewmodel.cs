﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Edaybook
{
    public class MainViewmodel : INotifyPropertyChanged
    {

        public MainViewmodel()
        {
            var getstuff = Refresh();
            Selectedclient = new Client();
        }

        private Stream imageStream;
        public Stream ImageStream
        {
            get { return imageStream; }
            set
            {
                imageStream = value;
                NotifyPropertyChanged("ImageStream");
            }
        }

        private Client selectedclient;
        public Client Selectedclient
        {
            get { return selectedclient; }
            set
            {
                selectedclient = value;
                NotifyPropertyChanged("Selectedclient");
            }
        }

        public IMobileServiceTable<Client> clienttable = App.MobileService.GetTable<Client>();

        private MobileServiceCollection<Client, Client> clientitems;
        public MobileServiceCollection<Client, Client> Clientitems
        {
            get { return clientitems; }
            set
            {
                clientitems = value;
                NotifyPropertyChanged("Clientitems");
            }
        }

        async Task Refresh()
        {
            Clientitems = await clienttable.ToCollectionAsync();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            try
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception e)
            {

            }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                NotifyPropertyChanged("Title");
            }
        }
        private string firstname;
        public string Firstname
        {
            get { return firstname; }
            set
            {
                firstname = value;
                NotifyPropertyChanged("Firstname");
            }
        }
        private string lastname;
        public string Lastname
        {
            get { return lastname; }
            set
            {
                lastname = value;
                NotifyPropertyChanged("Lastname");
            }
        }
        private string address;
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                NotifyPropertyChanged("Address");
            }
        }

        // Image
        string containerName;
        public string ContainerName
        {
            get { return containerName; }
            set
            {
                containerName = value;
                NotifyPropertyChanged("ContainerName");
            }
        }
        string resourceName;
        public string ResourceName
        {
            get { return resourceName; }
            set
            {
                resourceName = value;
                NotifyPropertyChanged("ResourceName");
            }
        }
        string sasQueryString;
        public string SasQueryString
        {
            get { return sasQueryString; }
            set
            {
                sasQueryString = value;
                NotifyPropertyChanged("SasQueryString");
            }
        }
        
        //string sasQueryString { get; set; }
        string imageUri { get; set; } 


        public async Task Insert()
        {
            // This code inserts a new Item into the database. When the operation completes
            // and Mobile Services has assigned an Id, the item is added to the CollectionView
            var client = new Client()
            {
                Title = Title,
                Firstname = Firstname,
                Lastname = Lastname,
                Address = Address
            };

            var hh = Selectedclient;
            string errorString = string.Empty;

            if (ImageStream != null)
            {
                // Set blob properties of TodoItem.
                client.ContainerName = "clientimages";
                client.ResourceName = Guid.NewGuid().ToString() + ".jpg";
            }

            // Send the item to be inserted. When blob properties are set this
            // generates an SAS in the response.
            await clienttable.InsertAsync(client);

            // If we have a returned SAS, then upload the blob.
            if (!string.IsNullOrEmpty(client.SasQueryString))
            {
                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                StorageCredentials cred = new StorageCredentials(client.SasQueryString);
                var imageUri = new Uri(client.ImageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                CloudBlobContainer container = new CloudBlobContainer(
                    new Uri(string.Format("https://{0}/{1}",
                        imageUri.Host, client.ContainerName)), cred);

                // Upload the new image as a BLOB from the stream.
                CloudBlockBlob blobFromSASCredential =
                    container.GetBlockBlobReference(client.ResourceName);
                await blobFromSASCredential.UploadFromStreamAsync(imageStream.AsInputStream());

                // When you request an SAS at the container-level instead of the blob-level,
                // you are able to upload multiple streams using the same container credentials.

                ImageStream = null;
            }
        }

        public async Task Update()
        {
            var client = new Client()
            {
                Title = Title,
                Firstname = Firstname,
                Lastname = Lastname,
                Address = Address
            };

            if (ImageStream != null)
            {
                // Set blob properties of TodoItem.
                client.ContainerName = "clientimages";
                //client.ResourceName = Guid.NewGuid().ToString() + ".jpg";
            }

            // Send the item to be inserted. When blob properties are set this
            // generates an SAS in the response.
            await clienttable.UpdateAsync(client);

            // If we have a returned SAS, then upload the blob.
            if (!string.IsNullOrEmpty(client.SasQueryString))
            {
                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                StorageCredentials cred = new StorageCredentials(client.SasQueryString);
                var imageUri = new Uri(client.ImageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                CloudBlobContainer container = new CloudBlobContainer(
                    new Uri(string.Format("https://{0}/{1}",
                        imageUri.Host, client.ContainerName)), cred);

                // Upload the new image as a BLOB from the stream.
                CloudBlockBlob blobFromSASCredential =
                    container.GetBlockBlobReference(client.ResourceName);
                await blobFromSASCredential.UploadFromStreamAsync(imageStream.AsInputStream());

                // When you request an SAS at the container-level instead of the blob-level,
                // you are able to upload multiple streams using the same container credentials.

                ImageStream = null;
            }
        }

    }
}
