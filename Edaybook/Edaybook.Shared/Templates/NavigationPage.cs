﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI;

namespace Edaybook.Templates
{
    public class NavigationPage : Page
    {
        public NavigationPage()
        {
            Loaded += NavigationPage_Loaded;
        }

        //public FlipView navflipview { get; set; }

        private FlipView _navflipview;
        public FlipView Navflipview
        {
            get { return _navflipview; }
            set
            {
                _navflipview = value;
            }
        }

        void NavigationPage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            // Navigation controls go in the top app bar.
            AppBar navBar = new AppBar();
            //navBar.Background = new SolidColorBrush(new Color() { A = 255, R = 0, G = 178, B = 240 });
            navBar.Content = new NavigationControl();
            var nc = (NavigationControl)navBar.Content;
            nc.Activeflipview = Navflipview;
            this.TopAppBar = navBar;
        }
    }
}