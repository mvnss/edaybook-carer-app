﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Edaybook.Pages
{
    public sealed partial class Home : Page
    {
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var whichbutton = (Button)sender;

            if ((String)whichbutton.Content == "Clients")
                this.Frame.Navigate(typeof(ClientGui));

            if ((String)whichbutton.Content == "Client Input")
                this.Frame.Navigate(typeof(ClientInput));
                
        }
    }
}
